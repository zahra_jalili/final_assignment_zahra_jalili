import unittest
import functionality
import pandas as pd
import numpy as np
import pandas.util.testing as pdTest


class TestFuctionalities(unittest.TestCase):
    def test_load(self):
        read = functionality.Read()
        df = read.read_csv_file("Data/heart.csv")
        df1 = pd.read_csv("Data/heart.csv")

        pdTest.assert_frame_equal(df1, df)

    def test_del_column(self):
        delete = functionality.Delete()
        df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                          columns=['a', 'b', 'c'])
        df_deleted = pd.DataFrame(np.array([[1, 2], [4, 5], [7, 8]]),
                                  columns=['a', 'b'])
        pdTest.assert_frame_equal(df_deleted, delete.del_col(df, ["c"]))

    def test_rename_col(self):
        rename = functionality.Rename()
        df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                          columns=['a', 'b', 'c'])
        df_renamed = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                                  columns=['Zahra', 'b', 'c'])
        pdTest.assert_frame_equal(df_renamed, rename.rename_col(df, {'a': 'Zahra'}))

    def test_change_type(self):
        join = functionality.Join()
        df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                          columns=['a', 'b', 'c'])
        df_changed = df
        df_changed['a'] = df['a'].astype(str)
        pdTest.assert_frame_equal(df_changed, join.changet_to_type(df, ['a'], str))

    def test_join(self):
        join = functionality.Join()
        df = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                          columns=['a', 'b', 'c'])
        df1 = pd.DataFrame(np.array([[7, 2, 3], [4, 5, 6], [1, 8, 9]]),
                           columns=['a', 'd', 'e'])
        df_joined = pd.DataFrame(np.array([[1, 2, 3, 8, 9], [4, 5, 6, 5, 6], [7, 8, 9, 2, 3]]),
                                 columns=['a', 'b', 'c', 'd', 'e'])
        pdTest.assert_frame_equal(df_joined, join.join_datasets(df, df1, ['a']))

    def test_dropna(self):
        datafrm = functionality.DataFrames()
        df = pd.DataFrame(np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [np.nan, 8.0, 9.0]]),
                          columns=['a', 'b', 'c'])
        df_dropped = pd.DataFrame(np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]),
                                  columns=['a', 'b', 'c'])
        pdTest.assert_frame_equal(df_dropped, datafrm.dropna(df, 'a'))

    def test_sort(self):
        datafrm = functionality.DataFrames()
        df = pd.DataFrame(np.array([[4.0, 5.0, 6.0], [1.0, 2.0, 3.0], [7.0, 8.0, 9.0]]),
                          columns=['a', 'b', 'c'])
        df_sorted = pd.DataFrame(np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]),
                                 columns=['a', 'b', 'c'], index=[1, 0, 2])
        pdTest.assert_frame_equal(df_sorted, datafrm.sort(df, 'a'))

    def test_groupby_mean(self):
        datafrm = functionality.DataFrames()
        df = pd.DataFrame(np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]),
                          columns=['a', 'b', 'c'])
        array = [[1.0, 4.0, 7.0], [2.0, 5.0, 8.0]]
        a = pd.MultiIndex.from_arrays(array, names=['a', 'b'])
        df_grouped = pd.DataFrame(np.array([[3.0], [6.0], [9.0]]),
                                  columns=['c'], index=a)

        pdTest.assert_frame_equal(df_grouped, datafrm.groupby_mean(df, ['a', 'b']))


if __name__ == "__main__":
    unittest.main()
